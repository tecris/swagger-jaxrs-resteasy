package org.terra.swagger.jaxrs.rest;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.terra.swagger.jaxrs.model.Movie;
import org.terra.swagger.jaxrs.model.Person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MovieEndpointIT {

    private static final String MOVIE_REST_URL = "http://localhost:8080/movie/rest/movies";

    private String personFirstName = "Dan";
    private String personLastName = "Kane";
    

    private String directorFirstName = "Orson";
    private String directorLastName = "W";


    @Test
    public void testGetMovie() throws JsonProcessingException {

        String expectedTitle = "B.D. la munte și la mare";
        String expectedPlot = "Brigada Diverse chasing drug dealers";
        String expectedCountry = "Romania";

        Long movieId = this.createMovie(expectedPlot, expectedCountry, expectedTitle).getId();

        get(MOVIE_REST_URL + "/" + movieId).then().body("title", equalTo(expectedTitle))
                .body("plot", equalTo(expectedPlot)).body("country", equalTo(expectedCountry));
    }

    @Test
    public void testUpdateMovie() throws JsonProcessingException {

        String expectedTitle = "B.D. la munte și la mare";
        String expectedPlot = "Brigada Diverse chasing drug dealers";
        String expectedCountry = "Romania";

        String updatedTitle = "B.D. in alerta";
        String updatedPlot = "Brigada Diverse after badies";
        String updatedCountry = "Republica Socialist Romania";

        Long movieId = this.createMovie(expectedPlot, expectedCountry, expectedTitle).getId();
        Movie movie = this.buildMovie(updatedPlot, updatedCountry, updatedTitle);
        movie.setId(movieId);
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(movie);
        given().contentType("application/json").body(jsonInString).when().put(MOVIE_REST_URL + "/" + movieId);

        get(MOVIE_REST_URL + "/" + movieId).then().body("title", equalTo(updatedTitle))
                .body("plot", equalTo(updatedPlot)).body("country", equalTo(updatedCountry));
    }

    @Test
    public void testDeleteMovie() throws JsonProcessingException {

        String expectedTitle = "B.D. la munte și la mare";
        String expectedPlot = "Brigada Diverse chasing drug dealers";
        String expectedCountry = "Romania";

        Long movieId = this.createMovie(expectedPlot, expectedCountry, expectedTitle).getId();
        delete(MOVIE_REST_URL + "/" + movieId);
        get(MOVIE_REST_URL + "/" + movieId).then().statusCode(404);
    }

    public Movie createMovie(String plot, String country, String title) throws JsonProcessingException {
        Movie movie = this.buildMovie(plot, country, title);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(movie);

        return given().contentType("application/json").body(jsonInString).when().post(MOVIE_REST_URL).as(Movie.class);
    }

    private Movie buildMovie(String plot, String country, String title) {
        Movie movie = new Movie();
        movie.setDirector(this.buildPerson(directorFirstName, directorLastName));
        movie.setPlot(plot);
        movie.setCountry(country);
        movie.setTitle(title);
        List<Person> cast = new ArrayList<>();
        cast.add(this.buildPerson(personFirstName, personLastName));
        movie.setCast(cast);
        return movie;
    }

    private Person buildPerson(String firstName, String lastName) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        return person;
    }
}
