package org.terra.swagger.jaxrs.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.terra.swagger.jaxrs.dao.MovieDao;
import org.terra.swagger.jaxrs.model.Movie;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Stateless
@Path("/movies")
@Api(value = "/movies")
public class MovieEndpoint {

    @Inject
    private MovieDao movieDao;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create movie", response = Movie.class)
    public Response create(@ApiParam(value = "Movie", required = true) Movie entity) {
        this.movieDao.create(entity);
        return Response.ok(entity).build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @ApiOperation(value = "Deletes movie with given id")
    public Response deleteById(@ApiParam(value = "Movie id", required = true) @PathParam("id") Long id) {
        this.movieDao.delete(id);
        return Response.noContent().build();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieves movie with given id")
    public Response findById(@ApiParam(value = "Movie id", required = true) @PathParam("id") Long id) {
        Movie entity = this.movieDao.findById(id);

        if (entity == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(entity).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Retrieves all movies")
    public List<Movie> listAll() {
        return this.movieDao.getAll();
    }

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update movie")
    public Response update(@ApiParam(value = "Movie id", required = true) @PathParam("id") Long id, @ApiParam(value = "Movie", required = true) Movie entity) {
        if (entity == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        if (id == null) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        if (!id.equals(entity.getId())) {
            return Response.status(Status.CONFLICT).entity(entity).build();
        }
        this.movieDao.update(entity);
        return Response.noContent().build();
    }
}
