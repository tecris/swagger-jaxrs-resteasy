package org.terra.swagger.jaxrs.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("/rest")
public class RestApplication extends Application {


    public RestApplication() {
        // https://github.com/swagger-api/swagger-core/wiki/Swagger-Core-RESTEasy-2.X-Project-Setup-1.5
        // http://localhost:8080/movie/rest/swagger.json
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setTitle("Swagger Demo");
        beanConfig.setDescription("JAXRS RESTEasy Demo");
        beanConfig.setBasePath("/movie/rest");
        beanConfig.setResourcePackage("org.terra.swagger.jaxrs");
        beanConfig.setScan(true);
        beanConfig.setPrettyPrint(true);
    }
}
