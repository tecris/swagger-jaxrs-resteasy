package org.terra.swagger.jaxrs.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "Movie", description = "Movie model representation")
public class Movie {

    @ApiModelProperty(value = "Movie's id", required = false)
    private Long id;

    @ApiModelProperty(value = "Movie's title", required = true)
    private String title;

    @ApiModelProperty(value = "Movie's id", required = true)
    private Person director;

    @ApiModelProperty(value = "Movie's plot", required = true)
    private String plot;

    @ApiModelProperty(value = "Movie's country", required = true)
    private String country;

    @ApiModelProperty(value = "Movie's cast", required = true, dataType = "List[org.terra.swagger.jaxrs.model.Person]")
    private List<Person> cast = new ArrayList<>();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Person> getCast() {
        return this.cast;
    }

    public void setCast(final List<Person> cast) {
        this.cast = cast;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) obj;
        if (id != null) {
            if (!id.equals(other.id)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (title != null && !title.trim().isEmpty())
            result += "name: " + title;
        if (plot != null && !plot.trim().isEmpty())
            result += ", plot: " + plot;
        if (country != null && !country.trim().isEmpty())
            result += ", country: " + country;
        return result;
    }

}
