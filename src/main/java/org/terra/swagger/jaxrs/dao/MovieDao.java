package org.terra.swagger.jaxrs.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.ejb.Stateless;

import org.terra.swagger.jaxrs.model.Movie;

/**
 * 
 */
@Stateless
public class MovieDao {

    private static final ConcurrentHashMap<Long, Movie> MOVIES = new ConcurrentHashMap<>();
    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    public void create(Movie movie) {
        movie.setId(ID_GENERATOR.getAndIncrement());
        MOVIES.put(movie.getId(), movie);
    }

    public Movie findById(Long pid) {
        return MOVIES.get(pid);
    }

    public void update(Movie movie) {
        if (findById(movie.getId()) != null) {
            MOVIES.put(movie.getId(), movie);
        }
    }

    public void delete(Long id) {
        MOVIES.remove(id);
    }

    public List<Movie> getAll() {
        return Collections.unmodifiableList(new ArrayList<>(MOVIES.values()));
    }
}
