## Swagger demo for JAX-RS RESTEasy

## Run demo

 ```
 $ docker-compose up -d
 $ mvn clean wildfly:deploy
 ```
 
 [Swagger UI](https://github.com/swagger-api/swagger-ui) @ http://localhost:81
